package com.example.RestApplication1.Model;

public class Employee {
    private Long Id;
    private String Name;
    private String mobileNo;
    private Long Salary;

    public Employee() {
    }

    public Employee(Long id, String name, String mobileNo, Long salary) {
        Id = id;
        Name = name;
        this.mobileNo = mobileNo;
        Salary = salary;
    }

    public Long getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public Long getSalary() {
        return Salary;
    }

    public void setId(Long id) {
        Id = id;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void setSalary(Long salary) {
        Salary = salary;
    }
}
