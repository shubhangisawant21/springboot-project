package com.example.RestApplication1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApplication1Application {

	public static void main(String[] args) {
		SpringApplication.run(RestApplication1Application.class, args);
	}

}
