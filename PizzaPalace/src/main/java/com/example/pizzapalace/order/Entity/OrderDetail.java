package com.example.pizzapalace.order.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long orderId;

    @Column(length = 30)
    private String Status;

    @Column(nullable = false)
    private int totalAmt;

    @DateTimeFormat
    @Column(nullable = false)
    private Date orderDate;

    @Column(nullable = false)
    private String deliveryStatus;

    @ManyToOne
    @JoinColumn(name = "customerId",nullable = false)
    private Customer customer;

//    @OneToMany
//    @JoinColumn(name="orderLineId",nullable=false)
//    private OrderLine orderLine;
}

