package com.example.pizzapalace.order.Service.Impl;

import com.example.pizzapalace.order.Entity.OrderDetail;
import com.example.pizzapalace.order.Repository.OrderDetailRepository;
import com.example.pizzapalace.order.Service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Override
    public OrderDetail save(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }
}
