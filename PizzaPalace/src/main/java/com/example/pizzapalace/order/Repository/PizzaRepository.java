package com.example.pizzapalace.order.Repository;

import com.example.pizzapalace.order.Entity.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PizzaRepository extends JpaRepository<Pizza,Long> {
}
