package com.example.pizzapalace.order.Service;

import com.example.pizzapalace.order.Entity.OrderDetail;
import org.springframework.stereotype.Service;

@Service
public interface OrderDetailService {
    public OrderDetail save(OrderDetail orderDetail);
}
