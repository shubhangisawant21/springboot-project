package com.example.pizzapalace.order.Service.Impl;

import com.example.pizzapalace.order.Entity.Pizza;
import com.example.pizzapalace.order.Repository.PizzaRepository;
import com.example.pizzapalace.order.Service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
public class PizzaServiceImpl implements PizzaService {
    @Autowired
    private PizzaRepository pizzaRepository;
    @Override
    public Pizza save(Pizza pizza) {
        return pizzaRepository.save(pizza);
    }

    @Override
    public List<Pizza> getAllPizza() {
        return pizzaRepository.findAll();
    }

    @Override
    public Optional<Pizza> getPizzaById(Long pizzaId) {
        return pizzaRepository.findById(pizzaId);
    }

    @Override
    public Pizza updatePizza( Long pizzaId, Pizza pizza) {

        Pizza pizza1 =pizzaRepository.findById(pizzaId).orElseThrow( () -> new RuntimeException());
        pizza1.setPizzaName(pizza.getPizzaName());
        pizza1.setPizzaType(pizza.getPizzaType());
        pizza1.setImageUrl(pizza.getImageUrl());
        pizza1.setDescription(pizza.getDescription());
        pizza1.setPriceRegSize(pizza.getPriceRegSize());
        pizza1.setPriceMSize(pizza.getPriceMSize());
        pizza1.setPriceLSize(pizza.getPriceLSize());
        return pizzaRepository.save(pizza1);
    }
}
