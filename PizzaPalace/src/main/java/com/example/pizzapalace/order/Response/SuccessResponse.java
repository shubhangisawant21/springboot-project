package com.example.pizzapalace.order.Response;

import com.example.pizzapalace.order.Entity.Customer;
import lombok.*;
import org.springframework.core.io.buffer.DataBufferWrapper;
import org.springframework.http.HttpStatus;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuccessResponse {

    private boolean success;
    private String message;
    private Customer data;

}




