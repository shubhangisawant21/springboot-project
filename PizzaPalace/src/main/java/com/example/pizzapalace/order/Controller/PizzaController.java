package com.example.pizzapalace.order.Controller;

import com.example.pizzapalace.order.Entity.Pizza;
import com.example.pizzapalace.order.Service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;

    //Add Pizza
    @PostMapping("/pizza")
    public Pizza addPizza(@RequestBody Pizza pizza){
        return pizzaService.save(pizza);
    }

    //Get All Pizzas
    @GetMapping("/getpizza")
    public List<Pizza> getPizzas(){
        return pizzaService.getAllPizza();
    }

    //Get pizza By Id
    @GetMapping("/getpizzabyid/{id}")
    public Optional<Pizza> getPizzaById(@PathVariable("id") Long pizzaId){
        return pizzaService.getPizzaById(pizzaId);
    }

    //Update pizza

    @PutMapping("/{id}")
    public Pizza updatepizza(@PathVariable("id") Long pizzaId,@RequestBody Pizza pizza){
        return pizzaService.updatePizza(pizzaId,pizza);

    }





}
