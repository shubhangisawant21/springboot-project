package com.example.pizzapalace.order.Controller;

import com.example.pizzapalace.order.Entity.Customer;
import com.example.pizzapalace.order.Response.FailureResponse;
import com.example.pizzapalace.order.Response.SuccessResponse;
import com.example.pizzapalace.order.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    //Create customers
    @PostMapping("/customers")
    public ResponseEntity<SuccessResponse> createCustomer(@RequestBody Customer customer) {
        Customer cstmr = customerService.save(customer);
        SuccessResponse reponse = new SuccessResponse();
        boolean success;
//        SuccessResponse response = SuccessResponse.builder().message("Customer Created Succesfully").success(true).httpStatus(HttpStatus.OK).data(cstmr).build();
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
        reponse.setSuccess(true);
        reponse.setMessage("Customer created successfully");
        reponse.setData(cstmr);

        return new ResponseEntity<>(reponse, HttpStatus.CREATED);

    }

    //Get All customers
    @GetMapping("/getcustomers")
    public List<Customer> getCustomers() {
        return customerService.getCustomers();

    }

    //Get customers by Id
    @GetMapping("/getCustomers/{id}")
    public Optional<Customer> getCustomerById(@PathVariable("id") Long customerId) {
        return customerService.getCustomersById(customerId);
    }

    @PutMapping("/updateCustomer/{id}")
    public Customer updateCustomer(@PathVariable("id") Long customerId, @RequestBody Customer customer) {
        return customerService.updateCustomer(customerId, customer);
    }

    @DeleteMapping("/deletecustomer/{id}")
    public String deleteCustomer(@PathVariable("id") Long customerId) {
        customerService.deleteCustomerById(customerId);
        return "Customer deleted successfully";

    }


}
