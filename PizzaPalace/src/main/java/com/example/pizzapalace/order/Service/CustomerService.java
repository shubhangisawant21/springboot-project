package com.example.pizzapalace.order.Service;

import com.example.pizzapalace.order.Entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CustomerService {
    public Customer save(Customer customer);

    public List<Customer> getCustomers();

    public Optional<Customer> getCustomersById(Long customerId);

    public Customer updateCustomer(Long customerId, Customer customer);

    public void deleteCustomerById(Long customerId);
}
