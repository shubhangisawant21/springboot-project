package com.example.pizzapalace.order.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class OrderLine {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int orderLineId;

//    @OneToMany
//    @JoinColumn(name="orderId",nullable = false)
//    private Order order;

//    @ManyToOne
//    @JoinColumn(name="pizzaId")
//    private Pizza pizza;

    @Column(nullable = false,length=30)
    private String pizzaSize;

    @Column(nullable = false)
    private int quantity;

    @Column(nullable = false)
    private int totalPrice;

}
