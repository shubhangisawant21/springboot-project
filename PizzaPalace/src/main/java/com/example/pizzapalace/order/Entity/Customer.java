package com.example.pizzapalace.order.Entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long customerId;

    @Column(nullable = false, length = 30)
    private String firstName;

    @Column(length = 30)
    private String lastName;

    @Column(length = 255)
    private String address;

    @Column(length = 20)
    private String phoneNo;

    @Column(length = 70)
    private String emailId;

//    @JoinColumn
//    private Order order;


}
