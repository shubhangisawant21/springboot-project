package com.example.pizzapalace.order.Service;

import com.example.pizzapalace.order.Entity.Pizza;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PizzaService {

    public Pizza save(Pizza pizza);

    public List<Pizza> getAllPizza();

    public Optional<Pizza> getPizzaById(Long pizzaId);

    public Pizza updatePizza(Long pizzaId, Pizza pizza);
}
