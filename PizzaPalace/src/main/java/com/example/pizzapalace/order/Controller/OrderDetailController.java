package com.example.pizzapalace.order.Controller;

import com.example.pizzapalace.order.Entity.OrderDetail;
import com.example.pizzapalace.order.Service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderDetailController {

    @Autowired
    private OrderDetailService orderDetailService;

    @PostMapping("/orders")
    public OrderDetail createOrder(@RequestBody OrderDetail orderDetail){
        return orderDetailService.save(orderDetail);
    }
}
