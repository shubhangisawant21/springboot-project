package com.example.pizzapalace.order.Service.Impl;

import com.example.pizzapalace.order.Entity.Customer;
import com.example.pizzapalace.order.Repository.CustomerRepository;
import com.example.pizzapalace.order.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getCustomers() {
        return (List<Customer>) customerRepository.findAll();
    }

    @Override
    public Optional<Customer> getCustomersById(Long customerId) {
        return customerRepository.findById(customerId);
    }

    @Override
    public Customer updateCustomer(Long customerId, Customer customer) {
        Customer cst=customerRepository.findById(customerId).get();
        if(Objects.nonNull(customer.getFirstName())&&!"".equalsIgnoreCase(customer.getFirstName())){
            cst.setFirstName(customer.getFirstName());
        }
        if(Objects.nonNull(customer.getLastName())&&!"".equalsIgnoreCase(customer.getLastName())){
            cst.setLastName(customer.getLastName());
        }
        if(Objects.nonNull(customer.getAddress())&&!"".equalsIgnoreCase(customer.getAddress())){
            cst.setAddress(customer.getAddress());
        }
        if(Objects.nonNull(customer.getPhoneNo())&&!"".equalsIgnoreCase(customer.getPhoneNo())){
            cst.setPhoneNo(customer.getPhoneNo());
        }
        if(Objects.nonNull(customer.getEmailId())&&!"".equalsIgnoreCase(customer.getEmailId())){
            cst.setEmailId(customer.getEmailId());
        }
        return customerRepository.save(cst);
    }

    @Override
    public void deleteCustomerById(Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
