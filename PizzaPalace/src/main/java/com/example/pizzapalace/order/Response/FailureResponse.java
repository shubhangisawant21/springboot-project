package com.example.pizzapalace.order.Response;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class FailureResponse {

    private boolean success;
    private String message;
    private Err error;

}
class Err {

    private String code;
    private String message;

}

