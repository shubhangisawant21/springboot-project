package com.example.pizzapalace.order.Entity;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Pizza {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false )
    private Long pizzaId;

    @Column(nullable = false,length = 50)
    private String pizzaName;

    @Column(length = 250)
    private String description;

    @Column(length = 10)
    private String pizzaType;


    @Column(length=250)
    private String imageUrl;

    @Column(nullable = false)
    private Double priceRegSize;

    @Column(nullable = false)
    private Double priceMSize;

    @Column(nullable = false)
    private Double priceLSize;

}
